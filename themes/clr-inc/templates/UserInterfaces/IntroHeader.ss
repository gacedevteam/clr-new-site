
<div class="row justify-content-center">
	<div class="col-12 col-lg-10">
		<div class="section-heading">
			<h2><% if $Title %>$Title<% else %>$Name<% end_if %></h2>
			$SubHeading
		</div>
	</div>
</div>